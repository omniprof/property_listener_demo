package com.kenfogel.property_listener_demo;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sample class that represents a model that needs to inform other objects when
 * it is changed. 
 *
 * @author neon
 *
 */
public class PropertyData implements Serializable {

    // Declare a private or protected Logger object initialized with the class's
    // name
    private final static Logger LOG = LoggerFactory.getLogger(PropertyData.class);

    private String theObservedData;

    private final PropertyChangeSupport mPcs = new PropertyChangeSupport(this);

    /**
     * Set the data as called from another object
     *
     * @param theData
     */
    public void setObservedData(String theData) {
        LOG.debug("setObservedData: " + theData);
        String oldTheData = theObservedData;
        theObservedData = theData;
        mPcs.firePropertyChange("observedData", oldTheData, theData);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        LOG.debug("addPropertyChangeListener");
        mPcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        LOG.debug("removePropertyChangeListener");
        mPcs.removePropertyChangeListener(listener);
    }

    /**
     * Get the data
     *
     * @return the observed data
     */
    public String getTheObservedData() {
        LOG.debug("getTheObservedData");
        return theObservedData;
    }
}
