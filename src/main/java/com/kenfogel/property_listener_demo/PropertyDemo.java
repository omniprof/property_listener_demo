package com.kenfogel.property_listener_demo;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Updated demo class that uses the PropertyChangeListener
 *
 * The version in observerdemo_deprecated uses the Observer/Observable class and
 * interface that has been deprecated in Java 9. In this version of that program
 * we are using the PropertyChangeListener, an interface attached to any class
 * that needs to listen. The data class no longer needs to implement Observable
 * and instead instantiates PropertyChangeSupport and two methods that allow a
 * listener to be added or removed.
 *
 * @author Ken Fogel
 *
 */
@SuppressWarnings("serial")
public class PropertyDemo extends JFrame {

    // Declare a private or protected Logger object initialized with the class's
    // name
    private final static Logger LOG = LoggerFactory.getLogger(PropertyData.class);

    private JLabel label;
    private JTextField textField;
    private PropertyData observedData;
    private ViewOne ovo;
    private ViewTwo ovt;

    /**
     * Constructor that creates a frame with a text field in it and then
     * registers the ObeservedData so that when this frame changes the data then
     * the other two frames are notified
     */
    public PropertyDemo() {
        super("Property Demo");
    }

    /**
     * Create the GUI and add the two View classes as listeners
     */
    public void perform() {
        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        label = new JLabel("Enter any text:");
        textField = new JTextField(10);
        textField.addActionListener(new MyTextListener());
        c.add(label);
        c.add(textField);

        // Instantiate the data
        observedData = new PropertyData();
        
        // Instantiate the other two frames
        ovo = new ViewOne();
        ovt = new ViewTwo();

        // Register the frames with the PropertyData
        observedData.addPropertyChangeListener(ovo);
        observedData.addPropertyChangeListener(ovt);

        // Provide an initial value
        observedData.setObservedData("Initial Text");

        setSize(275, 80);
        setResizable(false);
        centerScreen();
        setVisible(true);
    }

    /**
     * Center this frame on the display
     */
    private void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                (dim.height - abounds.height) / 2);
    }

    /**
     * The inner class that acts as the listener for the text field
     *
     * @author neon
     *
     */
    class MyTextListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            LOG.debug("actionPerfomed: " + textField.getText());
            observedData.setObservedData(textField.getText());
        }
    }

    /**
     * The main method
     *
     * @param args
     */
    public static void main(String args[]) {
        PropertyDemo od = new PropertyDemo();
        od.perform();
        od.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
