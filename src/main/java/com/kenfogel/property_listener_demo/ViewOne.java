package com.kenfogel.property_listener_demo;

import java.awt.Container;
import java.awt.FlowLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Frame class that is called whenever the model changes
 *
 * @author neon
 *
 */
@SuppressWarnings("serial")
public class ViewOne extends JFrame implements PropertyChangeListener {

    // Declare a private or protected Logger object initialized with the class's
    // name
    private final static Logger LOG = LoggerFactory.getLogger(PropertyData.class);

    private JLabel label;

    /**
     * Constructor that sets up the frame.
     */
    public ViewOne() {
        super("Observed View One");
        initialize();
    }

    private void initialize() {
        // Retrieve the content pane (panel) of the frame
        // Used when placing items directly in the frame
        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        // Set the initial value of the label
        label = new JLabel("Text to Come!");

        c.add(label);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(150, 80);
        setResizable(false);
        upperLeftScreen();
        setVisible(true);
    }

    /**
     * Place the frame in the upper left of the screen
     */
    private void upperLeftScreen() {
        setLocation(1, 2);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String propertyName = evt.getPropertyName();
        
        LOG.debug("ViewOne propertyName: " + propertyName);

        if ("observedData".equals(propertyName)) {
            label.setText((String) evt.getNewValue());
        }
    }
}
